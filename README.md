# Elm todo app (elm 0.19)

Added functionality:
- Get all todos


Planned functionality:
- Add a todo
- Update a todo
- Delete a todo


Future maybes:
- Log in
- See history
- Filters and sorting
