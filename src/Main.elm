
module Main exposing (main)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Http
import Json.Decode as Decode exposing (Decoder, string, bool, list)
import Json.Decode.Pipeline exposing (required)
import Uuid exposing (Uuid, decoder, toString)



-- PORTS


-- MAIN

main = Browser.element
    { init = init
    , update = update
    , subscriptions = subscriptions
    , view = view}

-- MODEL

type alias Model =
    { todos : (List Todo)
    , errorMessage : Maybe String
    }


type alias Todo =
    { id : Uuid
    , description : String
    , completed : Bool
    }

init : () ->( Model, Cmd Msg )
init _ =
    ( { todos = []
      , errorMessage = Nothing
      }
    , httpCommand
    )

type Msg
    = SendHttpRequest
    | DataReceived (Result Http.Error (List Todo))


-- UPDATE
update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SendHttpRequest ->
            ( model, httpCommand )

        DataReceived (Ok todos) ->
            ( { model
                | todos = todos
                , errorMessage = Nothing
              }
            , Cmd.none
            )

        DataReceived (Err httpError) ->
            ( { model
                | errorMessage = Just (createErrorMessage httpError)
              }
            , Cmd.none
            )

createErrorMessage : Http.Error -> String
createErrorMessage httpError =
    case httpError of
        _ -> "Error getting todos... Try again later..."


httpCommand : Cmd Msg
httpCommand =
    Http.get { url = "http://localhost:3000/todos"
             , expect = Http.expectJson DataReceived ( list todoDecoder )
             }


todoDecoder : Decoder Todo
todoDecoder =
    Decode.succeed Todo
        |> required "id" decoder
        |> required "description" string
        |> required "completed" bool


view : Model -> Html Msg
view model =
    div []
        [ viewTodosOrError model
        ]

viewTodosOrError : Model -> Html Msg
viewTodosOrError model =
    case model.errorMessage of
        Just message ->
            viewError message

        Nothing ->
            viewTodos model.todos

viewError : String -> Html Msg
viewError errorMessage =
    let
        errorHandling =
            "Could not fetch todos at this time."
    in
        div []
            [ h3 [] [ text errorHandling ]
            , text ("Error: " ++ errorMessage)
            ]

viewTodos : List Todo -> Html Msg
viewTodos todos =
    div [ class "app"]
        [ h3 [ class "header"] [ text "Todos" ]
        , table [ class "main todos-element"]
            ([ viewTableHeader ] ++ List.map viewTodo todos)
        ]

viewTableHeader : Html Msg
viewTableHeader =
    tr [ class "todos-header"]
        [ th [ class "todos-header-update" ]
            [ text "" ]
        , th [ class "todos-header-description" ]
            [ text "Description" ]
        , th [ class "todos-header-completed" ]
            [ text "Completed" ]
        , th [ class "todos-header-delete" ]
            [ text "" ]
        ]

viewTodo : Todo -> Html Msg
viewTodo todo =
    let
        isCompleted =
            if todo.completed then
                "Completed"
            else
                "Not Completed"
    in
        tr [ class "todos-body"]
            [ td [ class "todos-body-update" ]
                [ button [] [ text "Update"] ]
            , td [ class "todos-body-description" ]
                [ text (todo.description) ]
            , td [ class "todos-body-completed" ]
                [ text (isCompleted) ]
            , td [ class "todos-body-delete" ]
                [ button [] [ text "delete" ] ]
            ]


-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.none

